from nltk import RegexpTokenizer

TOKENIZER_STRING = RegexpTokenizer("\d+.\d+|\d+|\w+|\\n|\\t|[<->]+|\"+|'+|#+|\s+|\D")
TOKENIZER_INDEX = RegexpTokenizer("\d+\.\d+|\d+|\w+|[<->]+|#+|\'+|\"+|[!-/]|[:-@]|[[-`]|[{-]|\\n")


def tokenize_string(string):
    tokens = TOKENIZER_STRING.tokenize(string)
    return tokens


class DocumentTokenizer:
    name = None
    tokens = []

    def __init__(self, file_path):
        self.name = file_path
        with open(file_path, 'r') as f:
            content = f.read()
        tokens = TOKENIZER_INDEX.tokenize(content)
        self.tokens = tokens

    def __str__(self):
        '''
        string interpritation of object
        call:str(instance)
        '''
        return self.name
